This repo permits to run a shiny server for the analyticyte application.

The current application is provided as a Dockerfile that could be launched on a cloud computing. As a user, the deployment section is probably what you only need to run the application. Other sections concerns the developpers and trainers. The base image section might interest them.

There are two stages, the base image creation and the application image creation. The base image freezes a Linux guest with R and all the packages required by the application. Packages installation is done once. It fasten the startup of an image for developing, but also for teaching.


## Deployment (IFB example)

Login to IFB, select a Shiny Server Machine, then launch it with advanced options.

Set the following configuration:

```
APP_STACK
SHINY_REPO
```

The cloud computer starts and runs the Dockerfile recipe that is based on the analyticyte_base image available on DockerHub. This process provides the application as a Web interface.

If you clone this repo, you can add packages to the guest using apt.txt and to the R installation using Install.R.


## Development

To develop and test new features in the application, use a Linux host with Docker installed and mimic the Dockerfile recipe.

Start analyticyte with the base image and a port for shiny web service. The base image is run in interactive mode, otherwise a shiny server is executed (see the FROM item). The base image is pulled from DockerHub on the first run because it does not exist in the local host cache.

```
docker run -it -p 3838:3838 samgg/analyticyte_base:0.2 bash
```

Then install the latest version of the analyticyte packages.

```
R -e "
Sys.setenv(R_REMOTES_UPGRADE = 'never')  # do not update packages from CRAN
remotes::install_gitlab('lohmann/projectsetup', host = 'https://gitcrcm.marseille.inserm.fr')
remotes::install_gitlab('lohmann/analytiCFX', host = 'https://gitcrcm.marseille.inserm.fr')
remotes::install_gitlab('lohmann/analyticyte', host = 'https://gitcrcm.marseille.inserm.fr')
"
```

Finally, launch the shiny process (with a log file).

```
R -e "options('shiny.port'=3838,shiny.host='0.0.0.0');library(analyticyte);analyticyte::run_app()" 2>&1 | tee 3838.log
```

Once done, exit the guest.

```
exit  # or Ctrl-D
```


## Teaching


In the guest, we can run many processes at once. Ports must be created before on the host. Run the guest in interactive mode, otherwise a shiny server is executed (see the FROM item).

```
docker run -it -p 3838:3838 -p 3839:3839 analyticyte_base bash
```

Install the latest packages of analyticyte.

Launch many processes.

```
R -e "options('shiny.port'=3838,shiny.host='0.0.0.0');library(analyticyte);analyticyte::run_app()" 2>&1 | tee 3838.log &
R -e "options('shiny.port'=3839,shiny.host='0.0.0.0');library(analyticyte);analyticyte::run_app()" 2>&1 | tee 3839.log &
```


## Base image

The base image freezes a Linux guest with R and all the packages required by the application. Packages installation is done once, which fasten the startup. The base image is also pushed on DockerHub for resuse. Here is how to create it.

Open a bash shell, clone the repo and build the base image.

```
git clone https://gitcrcm.marseille.inserm.fr/granjeaud/ifb_analyticyte
```

Build the base image.

```
docker build -f ifb_analyticyte/Dockerfile_base -t analyticyte_base .
```

Tag the build.

```
docker tag analyticyte_base samgg/analyticyte_base:0.2
```

Login dockerhub, then push the image (you must have an account on DockerHub).

```
docker login
docker push samgg/analyticyte_base:0.2
```

## Helping knowledge

### Connecting app and log directories to host
To expose a directory on the host to the container, use -v <host_dir>:<container_dir>. The following command will use one /srv/shinyapps as the Shiny app directory and /srv/shinylog as the directory for Shiny app logs. Note that if the directories on the host don't already exist, they will be created automatically.:

```
docker run --rm -p 3838:3838 \
    -v /srv/shinyapps/:/srv/shiny-server/ \
    -v /srv/shinylog/:/var/log/shiny-server/ \
    rocker/shiny
```

If you have an app in /srv/shinyapps/appdir, you can run the app by visiting http://localhost:3838/appdir/. (If using boot2docker, visit http://192.168.59.103:3838/appdir/)

In a real deployment scenario, you will probably want to run the container in detached mode (-d) and listening on the host's port 80 (-p 80:3838):

```
docker run -d -p 80:3838 \
    -v /srv/shinyapps/:/srv/shiny-server/ \
    -v /srv/shinylog/:/var/log/shiny-server/ \
    rocker/shiny
```

```
-d detached mode
-p 80:3838 redirect the guest port 3838 to the host 80
--rm run a temporary container
--user shiny run as user shiny
```
